# Proxydev
Create evdev proxy device for QEMU to allow:
1. Using built-in laptop keyboard inside virtual machines
2. Bluetooth mouse hot-plug support
3. Low input lag

## Getting started
Directory where this `README.md` file is located is defined as `${PROJECT_ROOT}`.

### Using make (other distributions)
Installation using make requires installing following dependencies.
#### Ubuntu
When installing on ubuntu
```shell
sudo apt-get install -y gcc git binutils pkg-config libsystemd-dev libevdev-dev
```
#### Common steps
1. Run `make proxydev && sudo make install` to compile and install.

### Using makepkg (ArchLinux)
Visit package site on [AUR](https://aur.archlinux.org/packages/proxydev)
1. Run `makepkg -csif` to compile and install

### Common steps
2. Use `sudo proxydev --list-devices` to find out vendor_id and product_id
have a look at `/etc/proxydev/config.toml.example`
```shell
cp /etc/proxydev/config.toml.example /etc/proxydev/config.toml
```
format of config file: `/etc/proxydev/config.toml`
```toml
[[ device ]]
device_type = "keyboard"
vendor_id = 0xdead
product_id = 0xbeef

[[ device ]]
device_type = "mouse"
vendor_id = 0xdead
product_id = 0xfeed

```
3. (optional) Additionally keyboard shortcuts may be added (see: `/etc/proxydev/config.toml`) (use `proxydev --list-keys` to get key names, evdev numerical codes can also be used)
```toml
[[ shortcut ]]
detect = "KEY_LEFTCTRL+KEY_F11"
generate =  "KEY_LEFTCTRL+KEY_RIGHTCTRL"

[[ shortcut ]]
detect = "KEY_LEFTCTRL+KEY_F12"
generate =  "KEY_NUMLOCK+KEY_F"

```
4. Run `systemctl enable --now proxydev.service` to enable service on system boot

5. Add device to Libvirt domain config.
```xml
<domain xmlns:qemu="http://libvirt.org/schemas/domain/qemu/1.0" type="kvm">
  <name>my-vm</name>
    ...
  <devices>
    ...
  </devices>
  <!-- start of relevant part -->
  <qemu:commandline>
    <qemu:arg value="-object"/>
    <qemu:arg value="input-linux,id=input1,evdev=/dev/input/by-id/proxydev,grab_all=yes,repeat=on"/>
  </qemu:commandline>
  <!-- end of relevant part-->
</domain>
```

6. Run libvirt domain and use combination of `LEFT-CTRL+RIGHT-CTRL` or configured shortcut in order to switch input between host and guest.

## Dependencies
Standard dependencies installed by default on most Linux distributions
* make, gcc and pkg-config
* systemd and libevdev
* tomlc99 library (`${PROJECT_ROOT}/vendor` folder)

## Versioning
Project uses [Semantic Versioning 2.0.0](https://semver.org/)

## License
Project is licensed under license described in `LICENSE` file.
