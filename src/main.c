#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <systemd/sd-device.h>
#include <systemd/sd-event.h>
#include <unistd.h>

#include "config.h"
#include "devmon.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

void joinAllThreads(struct main_data *m_data) {
  for (size_t i = 0; i < m_data->pDeviceLen; i++) {
    struct device devInfo = m_data->pDevice[i];
    for (size_t j = 0; j < devInfo.t_count; j++) {
      LOG_INFO("awaiting thread %ld\n", j);
      pthread_join(devInfo.t_pthread[j], NULL);
    }
  }
  LOG_INFO("all threads joined");
}

void help(int rc) {
  fprintf(stderr, "proxydev\n");
  fprintf(stderr, "available commands:\n");
  fprintf(stderr, "--list-devices : list all devices\n");
  fprintf(stderr, "--list-keys : list all keys\n");
  fprintf(stderr, "--config FILENAME : use specific config\n");
  fflush(stderr);
  exit(rc);
}

bool not_root() { return geteuid() != 0; }

int main(int argc, char **argv) {
  char *config = "/etc/proxydev/config.toml";
  LOG_DEBUG("argc = %d\n", argc);
  if (argc == 2) {
    if (strcmp(argv[1], "--list-devices") == 0) {
      if (not_root()) {
        LOG_ERROR("please run as root\n");
        exit(EXIT_FAILURE);
      }
      list_devices();
    }
    if (strcmp(argv[1], "--list-keys") == 0) {
      list_keys();
      exit(EXIT_SUCCESS);
    } else {
      LOG_ERROR("invalid argument\n");
      help(EXIT_FAILURE);
    }
  } else if (argc == 3) {
    if (strcmp(argv[1], "--config") == 0) {
      config = argv[2];
    } else {
      LOG_ERROR("invalid argument\n");
      help(EXIT_FAILURE);
    }
  } else if (argc != 1) {
    LOG_ERROR("invalid argument count\n");
    help(EXIT_FAILURE);
  }
  if (not_root()) {
    LOG_ERROR("please run as root\n");
    help(EXIT_FAILURE);
  }
  int err;

  struct main_data m_data;

  read_config(config, &m_data);

  struct libevdev *proxydev = create_proxydev();

  err = libevdev_uinput_create_from_device(
      proxydev, LIBEVDEV_UINPUT_OPEN_MANAGED, &m_data.uidev);

  if (err != 0) panic(EXIT_FAILURE, "unable to open uinput: %d", 2);

  run_threads_for_connected_devices(&m_data);

  sd_device_monitor *monitor_client = NULL;

  if ((err = sd_device_monitor_new(&monitor_client)) < 0) {
    panic(EXIT_FAILURE, "monitor_new failed: %d", err);
  }
  if ((err = sd_device_monitor_start(monitor_client, monitor_handler,
                                     (void *)&m_data) < 0)) {
    panic(EXIT_FAILURE, "sd_device_monitor_start failed: %d", err);
  }
  if ((err = sd_event_loop(sd_device_monitor_get_event(monitor_client))) < 0) {
    panic(EXIT_FAILURE, "sd_device_monitor_get_event failed: %d");
  }

  joinAllThreads(&m_data);
  libevdev_uinput_destroy(m_data.uidev);
  LOG_INFO("proxydev exiting\n");
  return EXIT_SUCCESS;
}
