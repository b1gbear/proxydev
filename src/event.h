#pragma once

#include <linux/input-event-codes.h>

#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

void list_keys();
const char *key_code_to_key_name_or_null_ptr(int evcode);
int key_name_to_key_code(const char *key_name);
void generateSequence(const struct libevdev_uinput *uidev, int *keys,
                      size_t keys_len);
void *passEvents_mouse(void *data);
void *passEvents_keyboard(void *data);
