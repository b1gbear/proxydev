#pragma once

#include <dirent.h>
#include <systemd/sd-device.h>
#include <systemd/sd-event.h>

#include "common.h"
#include "event.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

int no_dirs(const struct dirent *entry);

struct libevdev *create_proxydev();
int run_device(const char *path, struct main_data *m_data);
int monitor_handler(sd_device_monitor *m, sd_device *d, void *userdata);
int print_device(const char *path);
void list_devices();
void run_threads_for_connected_devices(struct main_data *m_data);
