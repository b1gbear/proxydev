#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void panic(int retcode, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vfprintf(stderr, fmt, args);
  va_end(args);
  fflush(stderr);
  exit(retcode);
}
