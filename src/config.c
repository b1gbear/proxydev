#include "config.h"

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "event.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"
#include "tomlc99/toml.h"

#define LINE_LENGTH 256

void trim_spaces(char *s) {
  char *d = s;
  while (1) {
    while (isspace(*d)) {
      ++d;
    }
    s = d;
    if (!s) {
      break;
    }
    s++;
    d++;
  }
}

size_t parse_shortcut(const char *line, int *key_list, size_t max_size,
                      const char *msg) {
  char line_buffer[LINE_LENGTH];
  memset(line_buffer, '\0', LINE_LENGTH);
  strncpy(line_buffer, line, LINE_LENGTH - 1);
  char *rest = line_buffer;
  size_t i = 0;
  char *token;
  while ((token = strtok_r(rest, "+", &rest)) && i < max_size) {
    int keycode_as_int = key_name_to_key_code(token);
    if (keycode_as_int) {
      key_list[i] = keycode_as_int;
    } else {
      char *endptr;
      int result = strtol(token, &endptr, 10);
      if (*endptr != '\0') {
        LOG_WARNING("value is not known key and not number %s", token);
        return 0;
      }
      key_list[i] = result;
    }
    i++;
  }
  LOG_TRACE("%x", i);
  return i;
}

void read_config(const char *filename, struct main_data *m) {
  struct device *pDevice = m->pDevice;
  FILE *fp;
  char errbuf[256];

  fp = fopen(filename, "r");
  if (!fp) {
    panic(EXIT_FAILURE, "cannot open config: %s", strerror(errno));
  }

  toml_table_t *conf = toml_parse_file(fp, errbuf, sizeof(errbuf));
  if (!conf) {
    panic(EXIT_FAILURE, "cannot parse file %s", errbuf);
  }

  fclose(fp);

  toml_array_t *device_array = toml_array_in(conf, "device");
  if (!device_array) {
    panic(EXIT_FAILURE, "cannot read device arrray");
  }
  int i;
  for (i = 0; i < CONFIG_MAX; i++) {
    toml_table_t *device = toml_table_at(device_array, i);
    if (!device) break;
    toml_datum_t vendor_id = toml_int_in(device, "vendor_id");
    if (!vendor_id.ok) {
      panic(EXIT_FAILURE, "cannot read vendor_id for device: %s", errbuf);
    }
    toml_datum_t product_id = toml_int_in(device, "product_id");
    if (!product_id.ok) {
      panic(EXIT_FAILURE, "cannot read product_id for device %s", errbuf);
    }
    toml_datum_t device_type = toml_string_in(device, "device_type");
    if (!device_type.ok) {
      panic(EXIT_FAILURE,
            "cannot read device_type for device expected mouse/keyboard: %s",
            errbuf);
    }
    if (strcmp("mouse", device_type.u.s) == 0) {
      pDevice[i].device_type = DEVICE_TYPE_MOUSE;
    } else if (strcmp("keyboard", device_type.u.s) == 0) {
      pDevice[i].device_type = DEVICE_TYPE_KEYBOARD;
    } else {
      panic(EXIT_FAILURE,
            "cannot read device_type for device expected mouse/keyboard");
    }
    pDevice[i].vendorId = vendor_id.u.i;
    pDevice[i].productId = product_id.u.i;
    pDevice[i].t_count = 0;
    LOG_INFO("config(%s): vendor_id=0x%04lx product_id=0x%04lx\n", filename,
             vendor_id.u.i, product_id.u.i);
  }
  m->pDeviceLen = i;

  toml_array_t *shortcut_array = toml_array_in(conf, "shortcut");
  if (shortcut_array) {
    int cc = 0;
    for (int j = 0; j < MAX_SHORTCUT_COUNT; j++) {
      toml_table_t *shortcut_entry = toml_table_at(shortcut_array, j);
      if (!shortcut_entry) {
        break;
      }
      toml_datum_t detect = toml_string_in(shortcut_entry, "detect");
      if (!detect.ok) {
        panic(EXIT_FAILURE, "cannot read detect for shortcut %s", errbuf);
      }
      toml_datum_t generate = toml_string_in(shortcut_entry, "generate");
      if (!generate.ok) {
        panic(EXIT_FAILURE, "cannot read detect for shortcut %s", errbuf);
      }
      int d = parse_shortcut(detect.u.s, m->shortcuts[cc].detect,
                             MAX_DETECT_LEN, "Parsing detect... ");
      int g = parse_shortcut(generate.u.s, m->shortcuts[cc].generate,
                             MAX_GENERATE_LEN, "Parsing generate... ");

      if (!d) {
        LOG_INFO("error when parsing detect\n");
      }
      if (!g) {
        LOG_INFO("error when parsing generate\n");
      }
      if (d && g) {
        LOG_INFO("adding keyboard shortcut\n");
        m->shortcuts[cc].generate_len = g;
        m->shortcuts[cc].detect_len = d;
        cc++;
      } else {
        LOG_INFO("skipping invalid keyboard shortcut\n");
      }
    }
    m->shortcuts_count = cc;
  }
}
