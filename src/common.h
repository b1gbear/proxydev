#pragma once

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "event.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

#define SMALL_BUFF_SIZE 128
#define DEVICE_MAX_THREADS_COUNT 16
#define MAX_SHORTCUT_COUNT 16
#define CONFIG_MAX 32

#define TRACE 0
#define DEBUG 0

#define LOG_COMMON(...)           \
  do {                            \
    fprintf(stderr, __VA_ARGS__); \
    fflush(stderr);               \
  } while (0)

#if DEBUG == 1
#define LOG_DEBUG(...) LOG_COMMON(__VA_ARGS__)
#define IF_DEBUG(x) x;
#else
#define LOG_DEBUG(...)
#define IF_DEBUG(x)
#endif

#if TRACE == 1
#define LOG_TRACE(...) LOG_COMMON(__VA_ARGS__)
#else
#define LOG_TRACE(...)
#endif

#define LOG_WARNING(...) LOG_COMMON(__VA_ARGS__)
#define LOG_INFO(...) LOG_COMMON(__VA_ARGS__)
#define LOG_ERROR(...) LOG_COMMON(__VA_ARGS__)

#define MAX_DETECT_LEN 4
#define MAX_GENERATE_LEN 5

struct key_name {
  const char *name;
  int code;
};

struct shortcut {
  size_t detect_len;
  int detect[MAX_DETECT_LEN];
  size_t generate_len;
  int generate[MAX_GENERATE_LEN];
};

typedef enum {
  THREAD_STATE_FINISHED = 0,
  THREAD_STATE_STARTED,
} THREAD_STATE;

typedef enum {
  DEVICE_TYPE_MOUSE = 0,
  DEVICE_TYPE_KEYBOARD,
} DEVICE_TYPE;

struct thread_arguments {
  struct libevdev_uinput *uidev;
  struct libevdev *dev;
  int fd;
  int *ret;
  struct shortcut *shortcuts;
  size_t shortcuts_count;
};

struct device {
  char name[SMALL_BUFF_SIZE];
  int vendorId;
  int productId;
  size_t t_count;
  DEVICE_TYPE device_type;
  int ret[DEVICE_MAX_THREADS_COUNT];
  struct thread_arguments t_args[DEVICE_MAX_THREADS_COUNT];
  pthread_t t_pthread[DEVICE_MAX_THREADS_COUNT];
};

struct main_data {
  struct libevdev_uinput *uidev;
  struct device pDevice[CONFIG_MAX];
  size_t pDeviceLen;
  struct shortcut shortcuts[MAX_SHORTCUT_COUNT];
  size_t shortcuts_count;
};

void panic(int retcode, const char *fmt, ...);
