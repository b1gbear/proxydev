#define _DEFAULT_SOURCE
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <systemd/sd-device.h>
#include <systemd/sd-event.h>
#include <unistd.h>

#include "common.h"
#include "event.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

static const char *DEV_INPUT_EVENT_PATH = "/dev/input/event";

int no_dirs(const struct dirent *entry) { return entry->d_type != DT_DIR; }

struct libevdev *create_proxydev() {
  struct libevdev *proxydev = libevdev_new();

  libevdev_set_name(proxydev, "proxydev");

  for (unsigned int key = 1; key < 255; ++key) {
    libevdev_enable_event_code(proxydev, EV_KEY, key, NULL);
  }

  libevdev_enable_event_type(proxydev, EV_REL);
  libevdev_enable_event_code(proxydev, EV_REL, REL_X, NULL);
  libevdev_enable_event_code(proxydev, EV_REL, REL_Y, NULL);
  libevdev_enable_event_code(proxydev, EV_SYN, SYN_REPORT, NULL);
  libevdev_enable_event_type(proxydev, EV_KEY);
  libevdev_enable_event_type(proxydev, 2);
  libevdev_enable_event_code(proxydev, 2, 11, NULL);
  libevdev_enable_event_code(proxydev, 2, 8, NULL);

  libevdev_enable_event_code(proxydev, EV_KEY, BTN_LEFT, NULL);
  libevdev_enable_event_code(proxydev, EV_KEY, BTN_RIGHT, NULL);
  libevdev_enable_event_code(proxydev, EV_KEY, 274, NULL);  // mouse button
  return proxydev;
}

int run_device(const char *path, struct main_data *m_data) {
  struct libevdev *dev;
  struct device *pDevice = m_data->pDevice;
  struct libevdev_uinput *uidev = m_data->uidev;
  size_t pDeviceLen = m_data->pDeviceLen;

  LOG_DEBUG("trying to create libevdev from %s\n", path);
  int fd = open(path, O_RDONLY | O_NONBLOCK);
  if (fd == -1) {
    LOG_DEBUG("cannot open fd for %s\n", path);
    return -1;
  }
  int rc = libevdev_new_from_fd(fd, &dev);
  if (rc < 0) {
    LOG_DEBUG("cannot create libevdev from %s\n", path);
    return -1;
  }
  int vendor = libevdev_get_id_vendor(dev);
  int product = libevdev_get_id_product(dev);

  LOG_INFO("dev: path: %s bus %#x vendor %#x product %#x, name %s\n", path,
           libevdev_get_id_bustype(dev), vendor, product,
           libevdev_get_name(dev));

  int detectedDeviceIndex = -1;
  for (size_t i = 0; i < pDeviceLen; i++) {
    struct device *singleDev = &pDevice[i];
    if (singleDev->vendorId == vendor && singleDev->productId == product) {
      detectedDeviceIndex = i;
      size_t k;
      for (k = 0; k < singleDev->t_count; k++) {
        if (singleDev->ret[k] == THREAD_STATE_FINISHED) {
          pthread_join(singleDev->t_pthread[k], NULL);
          break;
        }
      }
      if (k >= singleDev->t_count) {
        singleDev->t_count = k + 1;
      }
      snprintf(singleDev->name, SMALL_BUFF_SIZE, "%s", libevdev_get_name(dev));
      size_t t_count = k;
      singleDev->ret[t_count] = THREAD_STATE_STARTED;
      singleDev->t_args[t_count].uidev = uidev;
      singleDev->t_args[t_count].dev = dev;
      singleDev->t_args[t_count].fd = fd;
      singleDev->t_args[t_count].ret = &singleDev->ret[t_count];
      singleDev->t_args[t_count].shortcuts = m_data->shortcuts;
      singleDev->t_args[t_count].shortcuts_count = m_data->shortcuts_count;

      if (singleDev->device_type == DEVICE_TYPE_MOUSE) {
        LOG_INFO("Starting mouse handler for %s\n", path);
        pthread_create(&(singleDev->t_pthread[t_count]), NULL, passEvents_mouse,
                       (void *)&(singleDev->t_args[t_count]));
      } else {
        LOG_INFO("Starting keyboard handler for %s\n", path);
        pthread_create(&(singleDev->t_pthread[t_count]), NULL,
                       passEvents_keyboard,
                       (void *)&(singleDev->t_args[t_count]));
      }
    }
  }
  if (detectedDeviceIndex < 0) {
    close(fd);
    return 0;
  }
  return 0;
}

int monitor_handler(sd_device_monitor *m, sd_device *d, void *userdata) {
  sd_device_action_t action_t;
  const char *subsystem = "(empty)";
  const char *devname = "(empty)";
  const char *syspath = "(empty)";
  if (sd_device_get_action(d, &action_t) < 0) {
    LOG_DEBUG("monitor_handler rc=%d\n", 1);
    return 1;
  }
  if (action_t != SD_DEVICE_ADD) {
    LOG_DEBUG("monitor_handler rc=%d\n", 2);
    return 2;
  }
  if (sd_device_get_subsystem(d, &subsystem) < 0) {
    LOG_DEBUG("monitor_handler rc=%d\n", 3);
    return 3;
  }
  if (strcmp(subsystem, "input")) {
    LOG_DEBUG("monitor_handler rc=%d\n", 4);
    return 4;
  }
  if (sd_device_get_devname(d, &devname) < 0) {
    LOG_DEBUG("monitor_handler rc=%d\n", 5);
    return 5;
  }
  if (!strcmp(devname, "input")) {
    LOG_DEBUG("monitor_handler rc=%d\n", 6);
    return 6;
  }
  if (strncmp(DEV_INPUT_EVENT_PATH, devname, strlen(DEV_INPUT_EVENT_PATH))) {
    LOG_DEBUG("monitor_handler rc=%d\n", 7);
    return 7;
  }
  if (run_device(devname, (struct main_data *)userdata) >= 0) {
    if (sd_device_get_syspath(d, &syspath) >= 0) {
      LOG_INFO("adding: %s %s\n", devname, syspath);
    } else {
      LOG_INFO("adding: %s\n", devname);
    }
    return 0;
  }
  LOG_INFO("not adding: %s %s\n", devname, syspath);
  LOG_DEBUG("monitor_handler rc=%d\n", 1);
  return 8;
}

int print_device(const char *path) {
  struct libevdev *dev;
  int fd = open(path, O_RDONLY | O_NONBLOCK);
  if (fd == -1) {
    return -1;
  }

  int rc = libevdev_new_from_fd(fd, &dev);
  if (rc < 0) {
    return -1;
  }

  int vendor = libevdev_get_id_vendor(dev);
  int product = libevdev_get_id_product(dev);

  LOG_INFO("dev: path /dev/input/%s bus %#x vendor %#x product %#x, name %s\n",
           path, libevdev_get_id_bustype(dev), vendor, product,
           libevdev_get_name(dev));
  close(fd);
  return 0;
}

void list_devices() {
  printf("Printing list of connected devices:\n");
  struct dirent **namelist;
  int n;
  n = scandir("/dev/input", &namelist, no_dirs, NULL);
  if (n == -1) {
    panic(EXIT_FAILURE, "scandir");
  }
  chdir("/dev/input");

  while (n--) {
    print_device(namelist[n]->d_name);
  }
}

void run_threads_for_connected_devices(struct main_data *m_data) {
  int n;
  char device_path[256];
  struct dirent **namelist;

  n = scandir("/dev/input", &namelist, no_dirs, NULL);
  if (n < 0) {
    panic(EXIT_FAILURE, "scandir");
  }

  while (n--) {
    memset(device_path, '\0', 256);
    snprintf(device_path, 256 - 1, "/dev/input/%.128s", namelist[n]->d_name);
    LOG_TRACE("Run device on start %s\n", buff);
    run_device(device_path, m_data);
  }
  free(namelist);
}
