#include "event.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "common.h"
#include "libevdev/libevdev-uinput.h"
#include "libevdev/libevdev.h"

#define key_press(ev) ev.type == EV_KEY &&ev.value == 1
#define key_release(ev) ev.type == EV_KEY &&ev.value == 0
#define debug_print_key(ev)                                      \
  do {                                                           \
    LOG_TRACE("[event] %u %u %d\n", ev.type, ev.code, ev.value); \
    fflush(stdout);                                              \
  } while (0)

#define MAX_EVENTS 10
#define key_array_size 256
#define is_valid_keycode(key) (key >= 0 && key < 256)

static char is_key_pressed[512] = {0};

#define clear_key_pressed() memset(is_key_pressed, 0, key_array_size)

struct key_name key_name_array[] = {
    {.name = "KEY_ESC", .code = KEY_ESC},
    {.name = "KEY_1", .code = KEY_1},
    {.name = "KEY_2", .code = KEY_2},
    {.name = "KEY_3", .code = KEY_3},
    {.name = "KEY_4", .code = KEY_4},
    {.name = "KEY_5", .code = KEY_5},
    {.name = "KEY_6", .code = KEY_6},
    {.name = "KEY_7", .code = KEY_7},
    {.name = "KEY_8", .code = KEY_8},
    {.name = "KEY_9", .code = KEY_9},
    {.name = "KEY_0", .code = KEY_0},
    {.name = "KEY_MINUS", .code = KEY_MINUS},
    {.name = "KEY_EQUAL", .code = KEY_EQUAL},
    {.name = "KEY_BACKSPACE", .code = KEY_BACKSPACE},
    {.name = "KEY_TAB", .code = KEY_TAB},
    {.name = "KEY_Q", .code = KEY_Q},
    {.name = "KEY_W", .code = KEY_W},
    {.name = "KEY_E", .code = KEY_E},
    {.name = "KEY_R", .code = KEY_R},
    {.name = "KEY_T", .code = KEY_T},
    {.name = "KEY_Y", .code = KEY_Y},
    {.name = "KEY_U", .code = KEY_U},
    {.name = "KEY_I", .code = KEY_I},
    {.name = "KEY_O", .code = KEY_O},
    {.name = "KEY_P", .code = KEY_P},
    {.name = "KEY_LEFTBRACE", .code = KEY_LEFTBRACE},
    {.name = "KEY_RIGHTBRACE", .code = KEY_RIGHTBRACE},
    {.name = "KEY_ENTER", .code = KEY_ENTER},
    {.name = "KEY_LEFTCTRL", .code = KEY_LEFTCTRL},
    {.name = "KEY_A", .code = KEY_A},
    {.name = "KEY_S", .code = KEY_S},
    {.name = "KEY_D", .code = KEY_D},
    {.name = "KEY_F", .code = KEY_F},
    {.name = "KEY_G", .code = KEY_G},
    {.name = "KEY_H", .code = KEY_H},
    {.name = "KEY_J", .code = KEY_J},
    {.name = "KEY_K", .code = KEY_K},
    {.name = "KEY_L", .code = KEY_L},
    {.name = "KEY_SEMICOLON", .code = KEY_SEMICOLON},
    {.name = "KEY_APOSTROPHE", .code = KEY_APOSTROPHE},
    {.name = "KEY_GRAVE", .code = KEY_GRAVE},
    {.name = "KEY_LEFTSHIFT", .code = KEY_LEFTSHIFT},
    {.name = "KEY_BACKSLASH", .code = KEY_BACKSLASH},
    {.name = "KEY_Z", .code = KEY_Z},
    {.name = "KEY_X", .code = KEY_X},
    {.name = "KEY_C", .code = KEY_C},
    {.name = "KEY_V", .code = KEY_V},
    {.name = "KEY_B", .code = KEY_B},
    {.name = "KEY_N", .code = KEY_N},
    {.name = "KEY_M", .code = KEY_M},
    {.name = "KEY_COMMA", .code = KEY_COMMA},
    {.name = "KEY_DOT", .code = KEY_DOT},
    {.name = "KEY_SLASH", .code = KEY_SLASH},
    {.name = "KEY_RIGHTSHIFT", .code = KEY_RIGHTSHIFT},
    {.name = "KEY_KPASTERISK", .code = KEY_KPASTERISK},
    {.name = "KEY_LEFTALT", .code = KEY_LEFTALT},
    {.name = "KEY_SPACE", .code = KEY_SPACE},
    {.name = "KEY_CAPSLOCK", .code = KEY_CAPSLOCK},
    {.name = "KEY_F1", .code = KEY_F1},
    {.name = "KEY_F2", .code = KEY_F2},
    {.name = "KEY_F3", .code = KEY_F3},
    {.name = "KEY_F4", .code = KEY_F4},
    {.name = "KEY_F5", .code = KEY_F5},
    {.name = "KEY_F6", .code = KEY_F6},
    {.name = "KEY_F7", .code = KEY_F7},
    {.name = "KEY_F8", .code = KEY_F8},
    {.name = "KEY_F9", .code = KEY_F9},
    {.name = "KEY_F10", .code = KEY_F10},
    {.name = "KEY_F11", .code = KEY_F11},
    {.name = "KEY_F11", .code = KEY_F12},
    {.name = "KEY_NUMLOCK", .code = KEY_NUMLOCK},
    {.name = "KEY_KP7", .code = KEY_KP7},
    {.name = "KEY_KP8", .code = KEY_KP8},
    {.name = "KEY_KP9", .code = KEY_KP9},
    {.name = "KEY_KPMINUS", .code = KEY_KPMINUS},
    {.name = "KEY_KP4", .code = KEY_KP4},
    {.name = "KEY_KP5", .code = KEY_KP5},
    {.name = "KEY_KP6", .code = KEY_KP6},
    {.name = "KEY_KPPLUS", .code = KEY_KPPLUS},
    {.name = "KEY_KP1", .code = KEY_KP1},
    {.name = "KEY_KP2", .code = KEY_KP2},
    {.name = "KEY_KP3", .code = KEY_KP3},
    {.name = "KEY_KP0", .code = KEY_KP0},
    {.name = "KEY_KPDOT", .code = KEY_KPDOT},
    {.name = "KEY_F11", .code = KEY_F11},
    {.name = "KEY_F12", .code = KEY_F12},
    {.name = "KEY_KPENTER", .code = KEY_KPENTER},
    {.name = "KEY_RIGHTCTRL", .code = KEY_RIGHTCTRL},
    {.name = "KEY_KPSLASH", .code = KEY_KPSLASH},
    {.name = "KEY_SYSRQ", .code = KEY_SYSRQ},
    {.name = "KEY_RIGHTALT", .code = KEY_RIGHTALT},
    {.name = "KEY_HOME", .code = KEY_HOME},
    {.name = "KEY_UP", .code = KEY_UP},
    {.name = "KEY_PAGEUP", .code = KEY_PAGEUP},
    {.name = "KEY_LEFT", .code = KEY_LEFT},
    {.name = "KEY_RIGHT", .code = KEY_RIGHT},
    {.name = "KEY_END", .code = KEY_END},
    {.name = "KEY_DOWN", .code = KEY_DOWN},
    {.name = "KEY_PAGEDOWN", .code = KEY_PAGEDOWN},
    {.name = "KEY_INSERT", .code = KEY_INSERT},
    {.name = "KEY_DELETE", .code = KEY_DELETE},
};

size_t key_name_array_size = sizeof(key_name_array) / sizeof(key_name_array[0]);

void list_keys() {
  printf("Printing list of keynames and keycodes:\n");
  printf("%-16s = %5s\n", "key", "keycode");

  for (size_t i = 0; i < key_name_array_size; i++) {
    printf("%-16s = %5d\n", key_name_array[i].name, key_name_array[i].code);
  }
  fflush(stdout);
}

const char *key_code_to_key_name_or_null_ptr(int evcode) {
  for (size_t i = 0; i < key_name_array_size; i++) {
    if (key_name_array[i].code == evcode) {
      return key_name_array[i].name;
    }
  }
  return 0;
}

int key_name_to_key_code(const char *key_name) {
  for (size_t i = 0; i < key_name_array_size; i++) {
    if (strcmp(key_name_array[i].name, key_name) == 0) {
      return key_name_array[i].code;
    }
  }
  return 0;
}

void generateSequence(const struct libevdev_uinput *uidev, int *keys,
                      size_t keys_len) {
  LOG_TRACE("generating sequence");
  size_t i = 0;
  for (i = 0; i < keys_len; i++) {
    LOG_TRACE("shortcut pressing: %s\n",
              key_code_to_key_name_or_null_ptr(keys[i]));
    libevdev_uinput_write_event(uidev, EV_KEY, keys[i], 1);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
  }
  for (; i > 0; i--) {
    LOG_TRACE("shortcut releasing: %s\n",
              key_code_to_key_name_or_null_ptr(keys[i - 1]));
    libevdev_uinput_write_event(uidev, EV_KEY, keys[i - 1], 0);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
  }
  LOG_TRACE("done");
  fflush(stderr);
}

void *passEvents_mouse(void *data) {
  struct thread_arguments *t_args = data;
  libevdev_grab(t_args->dev, LIBEVDEV_GRAB);
  int rc, nfds, n;
  struct epoll_event ev;
  int epollfd = epoll_create1(0);
  if (epollfd == -1) {
    goto passEvents_mouse_finished_procedure;
  }
  ev.events = EPOLLIN;
  ev.data.fd = t_args->fd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, t_args->fd, &ev) == -1) {
    goto passEvents_mouse_finished_procedure;
  }
  struct epoll_event events[MAX_EVENTS];
  struct input_event evd;
  for (rc = 0; rc == 1 || rc == 0 || rc == -EAGAIN;) {
    nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
    if (nfds == -1) {
      break;
    }
    for (n = 0; n < nfds; ++n) {
      if (events[n].data.fd == t_args->fd) {
        while ((rc = libevdev_next_event(t_args->dev, LIBEVDEV_READ_FLAG_NORMAL,
                                         &evd)) == 0) {
          libevdev_uinput_write_event(t_args->uidev, evd.type, evd.code,
                                      evd.value);
        }
      }
    }
  }
passEvents_mouse_finished_procedure:
  *(t_args->ret) = THREAD_STATE_FINISHED;
  close(t_args->fd);
  return NULL;
}

void *passEvents_keyboard(void *data) {
  struct thread_arguments *t_args = data;

  libevdev_grab(t_args->dev, LIBEVDEV_GRAB);
  int rc, nfds, n;
  struct epoll_event ev;
  int epollfd = epoll_create1(0);
  if (epollfd == -1) {
    goto passEvents_finished_procedure;
  }
  ev.events = EPOLLIN;
  ev.data.fd = t_args->fd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, t_args->fd, &ev) == -1) {
    goto passEvents_finished_procedure;
  }
  clear_key_pressed();
  struct epoll_event events[MAX_EVENTS];
  struct input_event evd;
  for (rc = 0; rc == 1 || rc == 0 || rc == -EAGAIN;) {
    nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
    if (nfds == -1) {
      break;
    }
    for (n = 0; n < nfds; ++n) {
      if (events[n].data.fd == t_args->fd) {
        while ((rc = libevdev_next_event(t_args->dev, LIBEVDEV_READ_FLAG_NORMAL,
                                         &evd)) == 0) {
          bool skip_key_because_shortcut_was_generated = false;
          LOG_TRACE("shortcut count: %d\n", t_args->shortcuts_count);
          if (key_press(evd)) {
            for (size_t j = 0; j < t_args->shortcuts_count; j++) {
              struct shortcut sc = t_args->shortcuts[j];
              if (evd.code == sc.detect[sc.detect_len - 1]) {
                size_t k = 0;
                for (k = 0; k < sc.detect_len - 1; k++) {
                  LOG_TRACE("pressed %d\n", sc.detect[k]);
                  if (!is_valid_keycode(sc.detect[k])) {
                    break;
                  }
                  if (!is_key_pressed[sc.detect[k]]) {
                    break;
                  }
                }
                if (k == sc.detect_len - 1) {
                  for (k = 0; k < sc.generate_len; k++) {
                    if (is_valid_keycode(sc.generate[k]) &&
                        is_key_pressed[sc.generate[k]]) {
                      libevdev_uinput_write_event(t_args->uidev, EV_KEY,
                                                  sc.generate[k], 1);
                      libevdev_uinput_write_event(t_args->uidev, EV_SYN,
                                                  SYN_REPORT, 0);
                    }
                  }
                  skip_key_because_shortcut_was_generated = true;
                  generateSequence(t_args->uidev, sc.generate, sc.generate_len);
                }
              }
            }
          }
          if (!skip_key_because_shortcut_was_generated) {
            if (is_valid_keycode(evd.code)) {
              if (key_press(evd)) {
                is_key_pressed[evd.code] = 1;
              } else if (key_release(evd)) {
                is_key_pressed[evd.code] = 0;
              }
            }
            libevdev_uinput_write_event(t_args->uidev, evd.type, evd.code,
                                        evd.value);
          }
        }
      }
    }
  }
passEvents_finished_procedure:
  *(t_args->ret) = THREAD_STATE_FINISHED;
  close(t_args->fd);
  return NULL;
}
