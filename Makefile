D := $(if $(D),$(D),)
CFLAGS := -Wall -pedantic -Werror -O3 -std=gnu99
LIBS := `pkg-config --cflags --libs --shared libevdev` `pkg-config --cflags --libs --shared libsystemd`

all: proxydev

.PHONY: proxydev
proxydev: out/proxydev

out/common.o: src/common.c
	mkdir -p out
	$(CC) -c -Ivendor  -o $@ $< $(CFLAGS) $(LIBS)


out/devmon.o: src/devmon.c
	mkdir -p out
	$(CC) -c -Ivendor  -o $@ $< $(CFLAGS) $(LIBS)

out/event.o:  src/event.c src/event.h src/common.h
	mkdir -p out
	$(CC) -Ivendor  -c -o $@ $< $(CFLAGS) $(LIBS)

out/config.o: src/config.c src/config.h src/common.h
	mkdir -p out
	$(CC) -Ivendor -c -o $@ $< $(CFLAGS) $(LIBS)

out/proxydev:  out/common.o  out/config.o out/event.o out/devmon.o ./vendor/tomlc99/toml.c src/main.c
	mkdir -p out
	$(CC) -Ivendor -L. -Lout -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: run
run: out/proxydev
	out/proxydev

.PHONY: clean
clean:
	rm -rf out

# style=LLVM|GNU|Google|Chromium|Microsoft|Mozilla|WebKit
.PHONY: format
format:
	find src -type f -exec clang-format -i --style=Google {} +

.PHONY: install
install: 
	install -d $(D)/etc/proxydev/
	install -d $(D)/etc/udev/rules.d/
	install -d $(D)/usr/bin
	install -d $(D)/etc/systemd/system/
	install -m 644 pkg/etc/udev/rules.d/80-proxydev.rules $(D)/etc/udev/rules.d/80-proxydev.rules
	install -m 644 pkg/etc/proxydev/config.toml.example $(D)/etc/proxydev/config.toml.example
	install -m 644  pkg/usr/lib/systemd/system/proxydev.service $(D)/usr/lib/systemd/system/proxydev.service
	install out/proxydev $(D)/usr/bin/proxydev

.PHONY: uninstall
uninstall: 
	systemctl disable --now proxydev
	rm -rf $(D)/etc/systemd/system/proxydev.service $(D)/usr/bin/proxydev $(D)/etc/proxydev/config.toml.example $(D)/etc/udev/rules.d/80-proxydev.rules
